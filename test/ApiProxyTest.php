<?php

declare(strict_types=1);

namespace InsideApps\ApiProxy;


use InsideApps\ApiProxy\Factory\ProxyFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Dotenv\Dotenv;

class ApiProxyTest extends TestCase
{


    public function setUp(): void
    {
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../config/.env.local');
    }


    public function testApiProxy(): void
    {
        $apiProxyGesfor2 = ProxyFactory::bearerClient(
            baseUrl: $_ENV['GESFOR2_BASE_URL'],
            authUrl: $_ENV['GESFOR2_AUTH_URL'],
            credentialItems: [
                'username' => $_ENV['GESFOR2_USER'],
                'password' => $_ENV['GESFOR2_PASSWORD']
            ]
        );


        $gesforResponse = $apiProxyGesfor2->get('trabajador/dni/x9692580n', true, 10, false);
        $this->assertJson($gesforResponse);
    }


    public function testInsideappsPost(): void
    {
        $apiProxyInsideApps = ProxyFactory::bearerClient(
            baseUrl: $_ENV['INSIDE_BASE_URL'],
            authUrl: $_ENV['INSIDE_AUTH_URL'],
            credentialItems: [
                'username' => $_ENV['INSIDE_USER'],
                'password' => $_ENV['INSIDE_PASSWORD']
            ]
        );

        $data = <<<JSON

{
  "title": "some title",
  "content": "<h1>hi this is a content</h1>",
  "publicationDate": "2024-01-16 10:30",
  "username": "userTest1"
}

JSON;

        $insideResponse = $apiProxyInsideApps->post('wall/announcement/private', $data);

        $this->assertSame(200, $insideResponse->getStatusCode());
    }

    public function testInsideAppsHtmlGet(): void
    {
        $apiProxyInsideApps = ProxyFactory::bearerClient(
            baseUrl: $_ENV['INSIDE_BASE_URL'],
            authUrl: $_ENV['INSIDE_AUTH_URL'],
            credentialItems: [
                'username' => $_ENV['INSIDE_USER'],
                'password' => $_ENV['INSIDE_PASSWORD']
            ]
        );
        $apiProxyInsideApps->client()->refresh();


        $htmlResponse = $apiProxyInsideApps->get('project/issue/add/df/dffd/gh', true);
        $this->assertNotEmpty($htmlResponse);
    }


    public function testZoho(): void
    {
        $authUrl = sprintf(
            'https://accounts.zoho.com/oauth/v2/token?refresh_token=%s&client_id=%s&client_secret=%s&grant_type=refresh_token',
            $_ENV['ZOHO_REFRESH_TOKEN'],
            $_ENV['ZOHO_CLIENT_ID'],
            $_ENV['ZOHO_CLIENT_SECRET']
        );
        $apiProxyZoho = ProxyFactory::bearerClient(
            baseUrl: 'https://accounts.zoho.com',
            authUrl: $authUrl,
            credentialItems: [

            ],
            tokenResponse: 'access_token'
        );

        $token = $apiProxyZoho->client()->resolveToken();

        $this->assertNotEmpty($token);
    }

}
