<?php

declare(strict_types=1);

namespace InsideApps\ApiProxy\Client;


use InsideApps\ApiProxy\Client\Contract\ApiClient;
use InsideApps\ApiProxy\Client\Contract\BearerSettingsInterface;
use InsideApps\ApiProxy\Exception\ApiAuthentificationException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BearearClient extends AbstractClient implements ApiClient, Contract\BearerClient
{

    private string $tokenId;

    public function __construct(
        HttpClientInterface $httpClient,
        CacheInterface $cache,
        BearerSettingsInterface $bearerSettings
    ) {
        parent::__construct(
            httpClient:$httpClient,
            apiCredential: $bearerSettings,
            cache: $cache
        );
        $this->tokenId = $this->apiCredential->baseUrl();
    }


    public function options(): array
    {
        return [
            'auth_bearer' => $this->resolveToken(),
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ];
    }



    public function resolveToken(): string
    {
        return $this->cache->get($this->tokenId, function (ItemInterface $item): string {
            $item->expiresAfter($this->apiCredential->expiresAt());
            return $this->getToken();
        });
    }


    private function getToken(): string
    {

        $response = $this->httpClient->request('POST', $this->apiCredential->authUrl(), [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($this->apiCredential->credentialItems())
        ]);

        $statusCode = $response->getStatusCode();
        if ($statusCode !== 200) {
            throw new ApiAuthentificationException(sprintf('Error on auth: %s', $response->getContent(false)));
        }

        $token = $response->toArray()[$this->apiCredential->tokenResponse()];

        if (null === $token) {
            throw new ApiAuthentificationException(sprintf('Token string:%s not found', $this->apiCredential->tokenResponse()));
        }

        return $token;
    }


    public function refresh(): void
    {
        $this->cache->delete($this->tokenId);
    }

}