<?php

declare(strict_types=1);

namespace InsideApps\ApiProxy\Client;


use InsideApps\ApiProxy\Client\Contract\BearerSettingsInterface;

class BearerSettings implements BearerSettingsInterface
{

    private string $baseUrl;

    private string $authUrl;

    private int $expiresAt;

    private array $credentialItems;

    private string $tokenResponse;

    public function __construct(
        string $baseUrl,
        string $authUrl,
        array $credentialItems,
        int $expiresAt = 3600,
        string $tokenResponse = 'token'
    ) {
        $this->baseUrl = $baseUrl;
        $this->authUrl = $authUrl;
        $this->credentialItems = $credentialItems;
        $this->expiresAt = $expiresAt;
        $this->tokenResponse = $tokenResponse;
    }

    public function baseUrl(): string
    {
        return $this->baseUrl;
    }

    public function authUrl(): string
    {
        return $this->authUrl;
    }


    public function expiresAt(): int
    {
        return $this->expiresAt;
    }

    public function credentialItems(): array
    {

        return $this->credentialItems;
    }

    public function tokenResponse(): string
    {
        return $this->tokenResponse;
    }


}