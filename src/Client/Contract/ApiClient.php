<?php

namespace InsideApps\ApiProxy\Client\Contract;

use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface ApiClient
{

    public function options(): array;

    public function settings():BearerSettingsInterface;

    public function httpClient(): HttpClientInterface;


    public function cache(): CacheInterface;


}