<?php

namespace InsideApps\ApiProxy\Client\Contract;

interface BearerSettingsInterface
{

    public function baseUrl(): string;

    public function authUrl(): string;


    public function expiresAt(): int;

    public function credentialItems(): array;

    public function tokenResponse(): string;
}