<?php

namespace InsideApps\ApiProxy\Client\Contract;

interface BearerClient
{
    public function refresh():void;

    public function resolveToken():string;
}