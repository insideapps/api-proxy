<?php

declare(strict_types=1);

namespace InsideApps\ApiProxy\Client;

use InsideApps\ApiProxy\Client\Contract\BearerSettingsInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractClient
{

    public function __construct(
        protected readonly HttpClientInterface $httpClient,
        protected readonly BearerSettingsInterface $apiCredential,
        protected readonly CacheInterface $cache
    ){}


    public function httpClient(): HttpClientInterface
    {
        return $this->httpClient;
    }


    public function settings(): BearerSettingsInterface
    {
        return $this->apiCredential;
    }

    public function cache(): CacheInterface
    {
        return $this->cache;
    }



}