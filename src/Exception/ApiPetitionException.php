<?php

declare(strict_types=1);

namespace InsideApps\ApiProxy\Exception;

use RuntimeException;

class ApiPetitionException extends RuntimeException
{

}