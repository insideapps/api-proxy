<?php

declare(strict_types=1);

namespace InsideApps\ApiProxy\Factory;

use InsideApps\ApiProxy\ApiProxy;
use InsideApps\ApiProxy\Client\BearearClient;
use InsideApps\ApiProxy\Client\BearerSettings;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\NativeHttpClient;

class ProxyFactory
{

    public static function bearerClient(
        string $baseUrl,
        string $authUrl,
        array $credentialItems ,
        int $expiresAt = 3600,
        string $tokenResponse = 'token'
    ): ApiProxy {

        return new ApiProxy(
            client: new BearearClient(
                httpClient: new NativeHttpClient(),
                cache: new FilesystemAdapter(),
                bearerSettings: new BearerSettings(
                    $baseUrl,
                    $authUrl,
                    $credentialItems,
                    $expiresAt,
                    $tokenResponse
                )
            )
        );
    }


}