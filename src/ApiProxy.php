<?php

declare(strict_types=1);

namespace InsideApps\ApiProxy;

use InsideApps\ApiProxy\Client\Contract\ApiClient;
use InsideApps\ApiProxy\Client\Contract\BearerClient;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ApiProxy
{

    public function __construct(
        private readonly ApiClient | BearerClient $client
    ) {
    }


    public function get(
        string $url,
        bool $cached = false,
        int $expiresAt = 3600,
        bool $refresh = false
    ): ResponseInterface|string
    {
        if (false === $cached) {
            return $this->runRequest('GET', $url, []);
        }

        if($refresh){
            $this->cache()->delete($url);
        }

        return $this->cache()->get($url, function (ItemInterface $item) use ($url, $expiresAt) {
            $item->expiresAfter($expiresAt);
            return $this->runRequest('GET', $url, [])->getContent();
        });
    }

    public function delete(string $url): ResponseInterface
    {
        return $this->runRequest('DELETE', $url,[]);
    }

    public function post(string $url, string|array $data): ResponseInterface
    {
        return $this->runRequest('POST', $url, $data);
    }

    public function put(string $url, array $data = []): ResponseInterface
    {
        return $this->runRequest('PUT', $url, $data);
    }


    private function runRequest(string $method, string $url, array|string $data): ResponseInterface
    {
        $urlPetition = sprintf('%s/%s', $this->client->settings()->baseUrl(), $url);

        $options = $this->client->options();

        if('POST' === $method || 'PUT' === $method){
            $options['body'] = (is_array($data)) ?  json_encode($data) : $data;
        }

        return $this->client->httpClient()->request($method, $urlPetition, $options);
    }

    public function client(): ApiClient|BearerClient
    {
        return $this->client;
    }

    public function cache(): CacheInterface
    {
        return $this->client->cache();
    }



}