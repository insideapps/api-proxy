**API-PROXY**
==============================
---
Basic proxy for Symfony/HttpClient 5.4

Requirements
---

- php >= 8.1 
- symfony/http-client
- symfony/cache 
- symfony/dotenv

Usage
---

- For Bearer token based

```
$myApiProxy = ProxyFactory::bearerClient(
            baseUrl: 'https://api.example.es/',
            authUrl: 'https://api.example.es/auth',
            credentialItems: [
                'username' => 'username',
                'password' => 'password'
            ]
        );

$myApiResponse = $myApiProxy->get('endoint/someParam');


```

- To enable cache for GET requests (only for GET), set second optional parameter to true

```
...

$myApiResponse = $myApiProxy->get('endoint/someParam',true,60);


```

In this example cache is setted to true, and expires at 60 seconds,(3600 default)